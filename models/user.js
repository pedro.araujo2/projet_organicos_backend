const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    social : {
        type: String,
        required: true
    },

    cnpj : {
        type: String,
        required: true,
        unique: true
    },

    endereco: {
        type: String,
        required: true
    },

    cep: {
        type: String,
        required: true
    },
    
    cidade: {
        type: String,
        required: true
    },

    uf: {
        type: String,
        required: true
    },

    telefone: {
        type: String,
        required: true
    },
    
    email: {
        type: String,
        required: true,
        unique: true
    },

    senha: {
        type: String,
        required: true,
        select: false
    },

    is_active: {
        type: Boolean,
        default: true
    },
    
    is_admin: {
        type: Boolean,
        default: false
    },
    
    date: {
        type: Date,
        default: Date.now
    }

})

module.exports = mongoose.model('user', UserSchema);
